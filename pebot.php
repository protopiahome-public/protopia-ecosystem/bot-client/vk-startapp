<?php

use \Firebase\JWT\JWT;

error_reporting(0);
echo "ok";
flush();
function output_null(){}
ini_set("output_handler", "output_null");
ini_set("dipslay_errors", 0);
require_once("config.php");
require_once("vk_functions.php");
require_once("graphql_protopia.class.php");

$data = json_decode(file_get_contents('php://input'), true);
file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
file_put_contents(__DIR__ . "/log.txt", print_r($data, true), FILE_APPEND);

if ($data["secret"] != $vk_secret || $data["type"] != "message_new")
{
	die();
}
$data = $data["object"];

protopia_auth();

$data["text"] = preg_replace("#^(\/[a-z0-9\_]+)\@.+?bot#i", "\\1", $data["text"]);
session_id("pebot" . $data["from_id"]);
session_start();

$vk_user = get_user($data["peer_id"], $data["from_id"]);
$vk_chat = get_chat($data["peer_id"]);

$graphql_protopia = new graphql_protopia($ecosystem_addr, $ecosystem_client_id, $ecosystem_client_url, $ecosystem_client_secret, "telegram", $data["from_id"]);
if ($graphql_protopia->ecosystem_user_token) {
	$user = $graphql_protopia->protopia_query("userInfo", "roles");
}

if(!$graphql_protopia->ecosystem_user_token)
{
	$user = $graphql_protopia->protopia_mutation("registerUser", "_id", ["input: UserInput" => [
		"name" => $vk_user["profile"]["first_name"],
		"family_name" => $vk_user["profile"]["last_name"],
		"vk_id" => $data["from_id"],
	]]);
	$graphql_protopia = new graphql_protopia($ecosystem_addr, $ecosystem_client_id, $ecosystem_client_url, $ecosystem_client_secret, "telegram", $data["from_id"]);
}

$chat_object = $graphql_protopia->protopia_query("getChatByExternal", "_id", ["input:ExternalInput" => [
	"external_id" => $data["peer_id"],
	"external_system" => "telegram",
	"external_type" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
]]);

if (!$chat_object)
{
	$chat_object = $graphql_protopia->protopia_mutation("changeChat", "_id", ["input: ChatInput" => [
		"title" => $vk_chat["chat_settings"]["title"],
		"external_id" => $data["peer_id"],
		"external_system" => "telegram",
		"external_type" => $data["peer_id"] == $data["from_id"] ? "personal_chat" : "group_chat",
	]]);
}

session_write_close();